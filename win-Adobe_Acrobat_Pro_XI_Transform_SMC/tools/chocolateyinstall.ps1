﻿
$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$fileLocation = Join-Path $toolsDir 'CopyTransform.bat'
$url        = ''
$url64      = ''

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'EXE'
  url           = $url
  url64bit      = $url64
  file         = $fileLocation

  softwareName  = 'win-Adobe_Acrobat_Pro_XI_Transform_SMC*'

  checksum      = 'F6AA46E7DFC96839D82C1C28327E91D06084D626E33DA50DCFE1DBF27E1D41B8'
  checksumType  = 'sha256'
  checksum64    = 'F6AA46E7DFC96839D82C1C28327E91D06084D626E33DA50DCFE1DBF27E1D41B8'
  checksumType64= 'sha256'

  
  validExitCodes= @(0, 3010, 1641)
  silentArgs   = ''
}

Install-ChocolateyInstallPackage @packageArgs










    








